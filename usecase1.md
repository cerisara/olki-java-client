# Un accélérateur de recherche en deep learning

Un nouveau doctorant arrive: il développe une nouvelle fonction de cout
pour entraîner un réseau profond; il doit la valider sur une quinzaine de
corpus avec des résultats comparables à l'état de l'art pour espérer publier.

Les modèles état de l'art et les scripts d'apprentissage sont souvent disponibles,
mais pour reproduire leurs résultats, il faut les adapter à l'environnement GPU
disponible, ce qui peut demander de multiples itérations essais / erreur / débuggage.
Il faut de plus bien maîtriser le préprocessing des données, les scripts de scoring et
les procédures d'apprentissage. Tout cela pour 15 corpus, ce qui demande énormément de
temps et d'effort, et ne concerne pas directement le travail de recherche.

Le principal problème est que ce travail est inévitable sur chaque site, car les
environnements matériels et logiciels sont tous particuliers.
Une solution serait de centraliser au niveau national, sur Jean Zay par exemple, un unique
environnement GPU partagé par tous les chercheurs français.
Mais ceci impliquerait un accès garanti à tout chercheur français le souhaitant,
cela rendrait l'ensemble de la recherche totalement dépendante de ce point d'accès central (et vulnérable),
ce ne serait qu'une solution nationale et ne permettrait pas le partage international,
cela impliquerait des couts de maintenance/jouvence/sécurité/orchestration/passage à l'échelle exhorbitants,
notamment pour ne pas devenir un frein pour la recherche à cause de files d'attente trop longues
et de politiques d'accès discutables, et surtout, une telle approche centralisée irait à
l'encontre de l'évolution actuelle du domaine, qui tend vers plus de conscience écologique,
une réduction des coûts énergétiques, une protection des données privées et sensibles,
et une décentralisation des moyens de calcul.

La solution privilégiée ici abonde dans le sens de la décentralisation, en proposant d'exploiter le
middleware et standard fédéré développé dans la plateforme OLKi pour connecter les sites de calcul
répartis sur le territoire. Il faut alors prendre en compte les spécificités de chaque site, et partager
la connaissance à la fois au niveau des acteurs du site, et au niveau global pour ce qui concerne
les resources et méthodes indépendantes du site (e.g., les modèles, les corpus, les algorithmes).
La future plateforme OLKi pourrait servir de base logicielle standardisée pour construire une telle approche.

Le doctorant n'aurait plus alors qu'à réutiliser les scripts d'apprentissage adaptés à son site, s'ils sont
disponibles, et s'ils ne le sont pas, les construire et les partager pour d'autres chercheurs.

## Approche technique

- Le noeud OLKi conserve:
    - des corpus, qui sont en fait des liens vers une zone NAS gérée sur lully via FileBrowser
    - des modeles, qui sont aussi des liens vers la zone NAS
    - des scripts de train et test
- Un service Java sur lully ouvre une connection permanente pour récupérer les notifications de OLKi
    - Lorsqu'une telle notification correspond à l'ajout d'un fichier de commande dans le repo d'un script,
le service Java exécute les commandes sur lully, e.g., crée un dir, y copie les corpus, scripts et modeles
et lance un train.
    - les résultats et l'avancement du script sont accessibles via FileBrowser
    - les scripts sont exécutés dans un python env avec fairseq + keras + TF

