## What it is

ce client ne fait que acceder au backend OLKi en utilisant le graphql API.
Il se connecte également via SSH à un autre serveur java installé sur lully qui
exécute les commandes de ML, en lancant les scripts python.

## Notes perso

le stockage de gros modeles dans docker/olki n'est pas une bonne idée: il faut
prévoir plutôt un stockage géré en externe et OLKi contient des liens vers les gros fichiers.

On peut mettre dans le "repo" OLKi juste un lien vers un autre service de download de gros fichiers,
type "FileBrowser".

pour creer un tunnel et servir FileBrowser depuis lully via olkihost, faire:
ssh -N -R 9426:localhost:80 olkihost

