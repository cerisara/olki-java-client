package fr.cerisara.olki;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.BoxLayout;
import javax.swing.Box;
import javax.swing.JPanel;
import javax.swing.JList;
import javax.swing.DefaultListModel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

public class OlkiSwing {
    public static void main(String[] args) throws Exception {  
        JFrame f=new JFrame();
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JPanel v = new OlkiView();
        f.getContentPane().add(v);
        f.setSize(900,700);
        f.setVisible(true);
        f.show();
    }

    static void old() throws Exception {  
        String url = "https://olki-social.loria.fr/";
        {
            BufferedReader f = new BufferedReader(new FileReader("olki.cfg"));
            for (;;) {
                String s=f.readLine();
                if (s==null) break;
                if (s.startsWith("server ")) url = s.substring(7).trim();
            }
            f.close();
        }
        final Connector c = new Connector(url);

        JFrame f=new JFrame();
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Box b0 = Box.createHorizontalBox();
        f.getContentPane().add(b0);

        final DefaultListModel eltslist = new DefaultListModel();
        final JList res = new JList(eltslist);
        res.setFont(new Font("Arial", Font.PLAIN, 18));
        final JScrollPane sres = new JScrollPane(res);

        JButton blist=new JButton("list");
        blist.setFont(new Font("Arial", Font.PLAIN, 36));
        final ArrayList<String[]> corpus = new ArrayList<String[]>();
        blist.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    ArrayList<String[]> l = c.getList();
                    eltslist.clear();
                    for (String[] s: l) {
                        eltslist.addElement(s[0]);
                        corpus.add(s);
                    }
                    sres.repaint();
                } catch (Exception ee) {}
            }
        });
        JButton brun=new JButton("run");
        brun.setFont(new Font("Arial", Font.PLAIN, 36));
        brun.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Thread runner = new Thread(new Runnable() {
                        public void run() {
                        try {
                            int i = res.getSelectedIndex();
                            System.out.println("index: "+i);
                            eltslist.clear();
                            System.out.println("corpus size "+corpus.size());
                            String[] corps = corpus.get(i);
                            String corp = corps[1];
                            System.out.println("corpus: "+corp);
                            sres.repaint();
                            c.runCmd(corp, new Connector.LineHandler() {
                                public void gotLine(String s) {
                                    eltslist.addElement(s);
                                    System.out.println("got line "+s);
                                    sres.repaint();
                                }
                                public void ended() {
                                    System.out.println("ended ");
                                };
                            });
                            sres.repaint();
                        } catch (Exception ee) {}
                        }
                    });
                    runner.start();
                } catch (Exception ee) {}
            }
        });



        Box b1 = Box.createVerticalBox();
        b1.add(Box.createVerticalGlue());
        b1.add(blist);
        b1.add(Box.createVerticalGlue());
        b1.add(brun);
        b1.add(Box.createVerticalGlue());

        b0.add(Box.createHorizontalGlue());
        b0.add(b1);
        b0.add(Box.createHorizontalGlue());
        b0.add(sres);
        b0.add(Box.createHorizontalGlue());

        f.setSize(900,700);//400 width and 500 height  
        f.setVisible(true);//making the frame visible  
        f.show();
    }  

}

