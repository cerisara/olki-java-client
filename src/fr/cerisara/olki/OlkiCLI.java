package fr.cerisara.olki;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import org.json.*;
import java.util.ArrayList;

import org.apache.commons.lang.StringEscapeUtils;
import org.jsoup.select.Elements;
import org.jsoup.nodes.Document;
import org.jsoup.Jsoup;

/*
 * pour ajouter de nouvelles fonctions: https://olki-social.loria.fr/graphql
 * */

public class OlkiCLI {
    private String olkiurl;
    private static final String USER_AGENT = "Mozilla/5.0";
    private ArrayList<String> curlist;
    public static OlkiCLI main;

    public OlkiCLI(String url) {
        olkiurl = url;
        main = this;
    }

    private String downloadText(String url) throws Exception {
        StringBuilder sb = new StringBuilder();
        BufferedReader f = new BufferedReader(new InputStreamReader(new URL(url).openStream()));
        for (;;) {
            String s = f.readLine();
            if (s==null) break;
            sb.append(s+"\n");
        }
        f.close();
        return sb.toString();
    }

    public interface LineHandler {
        public void gotLine(String s);
        public void ended();
    }
    public void runCmd(final String scriptID, LineHandler hdl) throws Exception {
        String cmdline = "ssh lully /home/xtof/bin/olkiworker.sh "+scriptID;
        Process process = new ProcessBuilder(new String[] {"bash", "-c", cmdline})
            .redirectErrorStream(true)
            .start();
        BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()));
        String line = null;
        while ( (line = br.readLine()) != null ) hdl.gotLine(line);
        hdl.ended();
    }


    public void cmts(String id) throws Exception {
        System.out.println("getting activities "+id+" ...");
        String q = "{\"query\": \"{activitiesCorpus (pk:\\\""+id+"\\\") {edges {node {inboxItems {edges {node {url}}}}}}}\"}";
        /*
         * {"data":{"activitiesCorpus":{"edges":[{"node":{"inboxItems":{"edges":[]}}},{"node":{"inboxItems":{"edges":[]}}},{"node":{"inboxItems":{"edges":[{"node":{"url":"https://mastodon.etalab.gouv.fr/users/cerisara/statuses/105685331585445338"}}]}}}
         * */
        String res = sendPOST(q);
        try {
            JSONObject obj = new JSONObject(res);
            obj = obj.getJSONObject("data"); //.getJSONObject("corpus").getString("description");
            JSONArray arr = obj.getJSONObject("activitiesCorpus").getJSONArray("edges");
            for (int i=0;i<arr.length();i++) {
                obj = arr.getJSONObject(i).getJSONObject("node");
                JSONArray aarr = obj.getJSONObject("inboxItems").getJSONArray("edges");
                for (int j=0;j<aarr.length();j++) {
                    obj = aarr.getJSONObject(j).getJSONObject("node");
                    String s = obj.getString("url");
                    System.out.println("---------");
                    System.out.println(s);
                    System.out.println("---------");
                    s = sendGET(new URL(s));
                    /*
                     * on peut avoir differentes structures JSON, selon que le post vient de masto, OLKi, pleroma
                     * donc je cherche simplement directement le content sans passer par la structure
                     */
                    int k = s.indexOf("\"content\":");
                    if (k>=0) {
                        int kk = s.indexOf("\"",k+9);
                        if (kk>=0) {
                            int kkk= s.indexOf("\",",kk);
                            if (kkk>=0) {
                                String txt = s.substring(kk+1,kkk);
                                txt = StringEscapeUtils.unescapeJava(txt);
                                if (txt.startsWith("<p>")) {
                                    Document doc = Jsoup.parse(txt);
                                    Elements ps = doc.select("p");
                                    txt = ps.text();
                                } 
                                System.out.println(txt);
                            }
                        }
                    }
                    System.out.println();
                }
            }
        } catch (Exception e) {
            System.out.println("corpus not found"+e.toString());
        }
    }

    public void ssh(final String scriptID) throws Exception {
        String cmdline = "ssh lully /home/xtof/bin/olkiworker.sh "+scriptID;
        Process process = new ProcessBuilder(new String[] {"bash", "-c", cmdline})
            .redirectErrorStream(true)
            .start();
        BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()));
        String line = null;
        while ( (line = br.readLine()) != null )
            System.out.println(line);

        //There should really be a timeout here.
        // if (0 != process.waitFor()) return null;
    }

    public void getScriptFile(String id) throws Exception {
        String q = "{\"query\": \"{corpus (pk:\\\""+id+"\\\") {files{name,url}}}\"}";
        String res = sendPOST(q);
        JSONObject obj = new JSONObject(res);
        JSONArray fichs = obj.getJSONObject("data").getJSONObject("corpus").getJSONArray("files");
        for (int i=0;i<fichs.length();i++) {
            String s = fichs.getJSONObject(i).getString("name");
            if (s.endsWith(".sh")) { // select only python files here
                String url = fichs.getJSONObject(i).getString("url");
                System.out.println("downloading "+s);
                String fullurl = olkiurl+"/media/"+url;
                String sc = downloadText(fullurl);
                PrintWriter f = new PrintWriter(new FileWriter(s));
                f.write(sc);
                f.close();
            }
        }
    }

    public void desc(String id) throws Exception {
        System.out.println("getting corpus "+id+" ...");
        String q = "{\"query\": \"{corpus (pk:\\\""+id+"\\\") {title,description}}\"}";
        String res = sendPOST(q);
        try {
            JSONObject obj = new JSONObject(res);
            String desc = obj.getJSONObject("data").getJSONObject("corpus").getString("description");
            System.out.println(desc);
        } catch (Exception e) {
            System.out.println("corpus not found");
        }
    }
    public ArrayList<String[]> getList() throws Exception {
        String q;

        // get nb of corpora
        // q = "{\"query\": \"{corpora {totalCount}}\"}";
        // int n = obj.getJSONObject("data").getJSONObject("corpora").getInt("totalCount");
        // get first page of titles
        q = "{\"query\": \"{corpora {edges {node {title,description,pk}}}}\"}";

        String res = sendPOST(q);
        JSONObject obj = new JSONObject(res);
        JSONArray edges = obj.getJSONObject("data").getJSONObject("corpora").getJSONArray("edges");
        ArrayList<String[]> lres = new ArrayList<String[]>();
        for (int i=0;i<edges.length();i++) {
            String s = edges.getJSONObject(i).getJSONObject("node").getString("title");
            String id = edges.getJSONObject(i).getJSONObject("node").getString("pk");
            String[] x = new String[2];
            x[0] = s; x[1] = id;
            lres.add(x);
        }
        return lres;
    }


    public void list() throws Exception {
        String q;

        // get nb of corpora
        q = "{\"query\": \"{corpora {totalCount}}\"}";
        // int n = obj.getJSONObject("data").getJSONObject("corpora").getInt("totalCount");
        
        // get first page of titles
        q = "{\"query\": \"{corpora {edges {node {title,description,pk}}}}\"}";

        curlist = new ArrayList<String>();
        String res = sendPOST(q);
        JSONObject obj = new JSONObject(res);
        JSONArray edges = obj.getJSONObject("data").getJSONObject("corpora").getJSONArray("edges");
        for (int i=0;i<edges.length();i++) {
            String s = edges.getJSONObject(i).getJSONObject("node").getString("title");
            String id = edges.getJSONObject(i).getJSONObject("node").getString("pk");
            curlist.add(id);
            System.out.println("CORPUS "+Integer.toString(i)+" "+s+" "+id);
        }
    }

    private String sendGET(URL obj) throws IOException {
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("GET");
		con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("Accept", "application/activity+json");
		int responseCode = con.getResponseCode();
		// System.out.println("GET Response Code :: " + responseCode);
		if (responseCode == HttpURLConnection.HTTP_OK) { // success
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
			while ((inputLine = in.readLine()) != null) response.append(inputLine);
			in.close();
			return response.toString();
		} else {
			return "";
		}
	}

	private String sendPOST(String data) throws IOException {
		URL obj = new URL(olkiurl+"/graphql");
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/json; utf-8");
        con.setRequestProperty("Accept", "application/json");
		con.setRequestProperty("User-Agent", USER_AGENT);

		// For POST only - START
		con.setDoOutput(true);
		OutputStream os = con.getOutputStream();
		os.write(data.getBytes());
		os.flush();
		os.close();
		// For POST only - END

		int responseCode = con.getResponseCode();
		// System.out.println("POST Response Code :: " + responseCode);

		if (responseCode == HttpURLConnection.HTTP_OK) { //success
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			return response.toString();
		} else {
			System.err.println("POST request not worked");
            return "";
		}
	}

    public static void main(String args[]) throws Exception {
        String url = "https://olki-social.loria.fr/";
        try {
            BufferedReader f = new BufferedReader(new FileReader("olki.cfg"));
            for (;;) {
                String s=f.readLine();
                if (s==null) break;
                if (s.startsWith("server ")) {
                    url = s.substring(7).trim();
                }
            }
            f.close();
        } catch (Exception e) {}

        System.out.println("connecting to "+url);
        OlkiCLI c = new OlkiCLI(url);
        if (args.length>=1) {
            if (args[0].equals("list")) c.list();
            else if (args[0].equals("desc")) c.desc(args[1]);
            else if (args[0].equals("script")) c.getScriptFile(args[1]);
            else if (args[0].equals("ssh")) c.ssh(args[1]);
            else if (args[0].equals("cmts")) c.cmts(args[1]);
            else System.out.println("cmd unknown "+args[0]);
        } else if (args.length==0) {
            String usage = "<program> <command>\n";
            usage += "\n";
            usage += "Available commands:\n";
            usage += "list   : list corpora\n";
            usage += "desc <id>   : print description of a corpus\n";
            usage += "cmts <id>   : print comments for a corpus\n";
            // usage += "script <id> : download script\n";
            // usage += "ssh <id> : execute script on the OLKi server\n";
            System.out.println(usage);
        }
    }

}

