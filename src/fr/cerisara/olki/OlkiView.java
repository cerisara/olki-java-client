package fr.cerisara.olki;

import java.util.ArrayList;
import javax.swing.JPanel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JList;
import javax.swing.JTextArea;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.GroupLayout;
import static javax.swing.GroupLayout.Alignment.LEADING;
import static javax.swing.LayoutStyle.ComponentPlacement.RELATED;

public class OlkiView extends JPanel {
    private DefaultListModel eltslist = new DefaultListModel();
    private JList corpusList;
    private JScrollPane corpusListPane;
    private OlkiCLI main = new OlkiCLI("https://olki-social.loria.fr");
    JButton blist;

    public OlkiView() {
        final JPanel view = this;
        blist=new JButton("list corpus");
        blist.setFont(new Font("Arial", Font.PLAIN, 20));
        final ArrayList<String[]> corpus = new ArrayList<String[]>();
        blist.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    ArrayList<String[]> l = main.getList();
                    eltslist.clear();
                    for (String[] s: l) {
                        eltslist.addElement(s[0]);
                        corpus.add(s);
                    }
                    // view.validate();
                    view.repaint();
                } catch (Exception ee) {}
            }
        });

        corpusList = new JList(eltslist);
        corpusList.setFont(new Font("Arial", Font.PLAIN, 18));
        corpusListPane = new JScrollPane(corpusList);

        JTextArea desc = new JTextArea();
        desc.setBorder(BorderFactory.createLineBorder(Color.black));
        desc.setFont(new Font("Arial", Font.PLAIN, 18));

        GroupLayout gl = new GroupLayout(this);
        gl.setVerticalGroup(
                gl.createSequentialGroup()
                .addGroup(gl.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(blist))
                .addGroup(gl.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(corpusListPane)
                    .addComponent(desc))
                );

        gl.setHorizontalGroup(
                gl.createSequentialGroup()
                .addGroup(gl.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(blist)
                    .addComponent(corpusListPane))
                .addGroup(gl.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(desc))
                );

        gl.setAutoCreateGaps(true);
        gl.setAutoCreateContainerGaps(true);
        setLayout(gl);
    }
}

